package bdd.tests;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

public class LoginGmail {

    RemoteWebDriver driver;

    @Before("@Login") public void setUp() throws MalformedURLException {
        String device = System.getProperty("device");
        // switch between diffrent browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("Android")) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("deviceName", "Android Emulator");
            capabilities.setCapability("platformVersion", "5.0.0");
            capabilities.setCapability("app", "Browser");
            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        } else {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("deviceName", "iPhone 6");
            capabilities.setCapability("platformName", "iOS");
            capabilities.setCapability("platformVersion", "9.2");
            capabilities.setCapability("browserName", "safari");
            driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        }
    }

    @After("@Login") public void quit() {
        if (driver != null) {
            driver.quit();
        }
    }

    @Given("^the initial gmail homepage") public void the_initial_gmail_homepage()
        throws Throwable {
        driver.get("http://www.gmail.com");
    }


    @When("^I click login and fill my bad credentials$")
    public void i_click_login_and_fill_my_bad_credentials() throws Throwable {
        driver.findElement(By.id("Email")).sendKeys("appiumtecnocom@gmail.com");
        driver.findElement(By.id("next")).click();
        driver.findElement(By.cssSelector("#Passwd"), "appiumtecnocom");
        driver.findElement(By.cssSelector("#signIn")).click();
    }

    @When("^I click login and fill my good credentials$")
    public void i_click_login_and_fill_my_good_credentials() throws Throwable {
        driver.findElement(By.id("Email")).sendKeys("appiumtecnocom@gmail.com");
        driver.findElement(By.id("next")).click();
        driver.findElement(By.cssSelector("#Passwd"), "tecnocomappium");
        driver.findElement(By.cssSelector("#signIn")).click();
    }

    @Then("^I cannot access my account$") public void i_cannot_access_my_account() {

    }

    @Then("^I access my account$") public void i_access_my_account() {

    }

}
